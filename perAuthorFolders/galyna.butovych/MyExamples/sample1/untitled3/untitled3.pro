#-------------------------------------------------
#
# Project created by QtCreator 2015-06-11T20:05:30
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

DISTFILES += \
    frag.fsh \
    vert.vsh

RESOURCES += \
    resource.qrc
