#include <QDebug>


#include <QList>
#include <QLinkedList>
#include <QVector>
#include <QStack>
#include <QQueue>
#include <QSet>
#include <QMap>
#include <QMultiMap>
#include <QHash>
#include <QMultiHash>

#include <list>

#include <QString>

int main(int lArgc, char *lArgv[])
{

    // QList
    // ==========
//    qDebug() << "QList ---";

//    QList<QString> lList;
//    QString lStr1("s1");
//    QString lStr2("s2");

//    lList << lStr1 << lStr2;
//    lList.prepend(lStr1);
//    lList.append("el3");
//    lList << "el4";

//    qDebug() << lList.first();
//    qDebug() << lList.last();
//    qDebug() << lList.at(1);

//    lList.removeOne(lStr2);

//    int lCount = lList.removeAll(lStr1);

//    lList.removeFirst();
//    lList.removeLast();

//    qDebug() << lList.isEmpty();

//    lList += lStr1;
//    qDebug() << lList.size();


//    qDebug() << "=======================";

    // QLinkedList
    // ==========
//    qDebug() << "QLinkedList ---";

//    QLinkedList<int> lLinkedList;

//    lLinkedList << 1 << 2 << 3;

//    lLinkedList.prepend(1);
//    lLinkedList.append(4);

//    qDebug() << lLinkedList.first();
//    qDebug() << lLinkedList.last();

//    lLinkedList.removeOne(2);

//    lCount = lLinkedList.removeAll(1);

//    lLinkedList.removeFirst();
//    lLinkedList.removeLast();

//    qDebug() << lLinkedList.isEmpty();

//    lLinkedList += 5;
//    qDebug() << lLinkedList.size();

//    qDebug() << "=======================";


    // QLinkedList
    // ==========
    qDebug() << "QVector ----";

    // Пустий вектор
    QVector<QString> lEmptyVector;

    // Ініціалізуємо вектор
    // 200 двісті елементів
    // ініціалізуємо рядком "Initial string"
    QVector<QString> lVector(200, "Initial string");

    // Додати елемент на початок
    lVector.prepend("first_string");
    // ...у кінець
    lVector.append("at_end");

    // Залишає лише 3 елемента
    // у векторі
    lVector.resize(3);

    // Резервуємо пам'ять для 5 елементів
    lVector.reserve(5);

    QList<int> lList;
    QVector<int> lIntVector
        = QVector<int>::fromList(lList);


    std::vector<int> lVectorList;
    lIntVector = QVector<int>::fromStdVector(lVectorList);


    qDebug() << "=======================";

    return 0;


}
