#ifndef SOMEWIDGET_H
#define SOMEWIDGET_H

#include <QWidget>

class SomeWidget : public QWidget
{
    Q_OBJECT

public:
    SomeWidget(QWidget *parent = 0);
    ~SomeWidget();
};

#endif // SOMEWIDGET_H
