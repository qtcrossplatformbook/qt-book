#-------------------------------------------------
#
# Project created by QtCreator 2013-09-19T07:17:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ObjectHierarchy
TEMPLATE = app


SOURCES += main.cpp\
        somewidget.cpp

HEADERS  += somewidget.h
