#include "somewidget.h"

#include <QApplication>

#include <QLabel>
#include <QPushButton>

int main(int argc, char *argv[])
{
    QApplication lApplication(argc, argv);

     // (1) Пам’ять у динамічно-розподілюваній пам’яті (heap)
    SomeWidget *lSomeWidget = new SomeWidget(0);

    // Задаємо батьківський віджет з допомогою конструктора
    QLabel *lLabel = new QLabel(lSomeWidget);

    // Задаємо батьківський віджет з допомогою
    // методу QObject::setParent()
    QPushButton *lPushButton = new QPushButton;
    lPushButton->setParent(lSomeWidget);


     // (2) Пам’ять у стеку
//    SomeWidget lSomeWidget;

//    // Задаємо батьківський віджет з допомогою конструктора
//    QLabel *lLabel = new QLabel(&lSomeWidget);

//    // Задаємо батьківський віджет з допомогою
//    // методу QObject::setParent()
//    QPushButton *lPushButton = new QPushButton;
//    lPushButton->setParent(&lSomeWidget);

    lLabel->setObjectName("ChildLabel");
    lPushButton->setObjectName("ChildPushButton");

    lSomeWidget->dumpObjectTree();

    lLabel->setText("Label");
    lLabel->move(10, 10);
    lPushButton->setText("Button");
    lPushButton->move(50, 10);

    // (1) Пам’ять у динамічно-розподілюваній пам’яті (heap)
    lSomeWidget->resize(150, 50);
    lSomeWidget->show();

    // (2) Пам’ять у стеку
//    lSomeWidget.resize(150, 50);
//    lSomeWidget.show();

    int exitCode = lApplication.exec();

    // (1) Пам’ять у динамічно-розподілюваній пам’яті (heap)
    delete lSomeWidget;

    return exitCode;
}
