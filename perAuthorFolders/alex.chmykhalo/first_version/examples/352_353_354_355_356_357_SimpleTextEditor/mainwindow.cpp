#include "mainwindow.h"

// Файл згенерований uic
// під час обробки файлу форми
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QDir>

#include "settingsdialog.h"

#include <QSettings>

const QString SETTINGS_GROUP_VIEW("View");
const QString SETTING_SHOW_TOOLBAR("ShowToolbar");
const QString SETTING_SHOW_STATUS_BAR("ShowStatusBar");

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), // Створюємо об'єкт,який містить дизайн
                           // розроблений у QtDesigner
    mSettingsDialog(new SettingsDialog(this))
{
    ui->setupUi(this); // Застосовуємо дизайн, який ми створили у QtDesigner
                       // до поточного вікна

    // Задаємо комбінації клавіш для дій
    ui->actionUndo->setShortcut(QKeySequence());
    ui->actionRedo->setShortcut(QKeySequence::Redo);
    ui->actionCut->setShortcut(QKeySequence::Cut);
    ui->actionC_opy->setShortcut(QKeySequence::Copy);
    ui->action_Paste->setShortcut(QKeySequence::Paste);
    ui->actionSelect_all->setShortcut(QKeySequence::SelectAll);
    ui->action_New->setShortcut(QKeySequence::New);
    ui->action_Open->setShortcut(QKeySequence::Open);
    ui->action_Save->setShortcut(QKeySequence::Save);
    ui->action_Exit->setShortcut(QKeySequence::Quit);

    // Приєднуємо дії до створених слотів
    connect(ui->action_New, SIGNAL(triggered()),
            this, SLOT(slotNew()), Qt::UniqueConnection);
    connect(ui->action_Open, SIGNAL(triggered()),
            this, SLOT(slotOpen()), Qt::UniqueConnection);
    connect(ui->action_Save, SIGNAL(triggered()),
            this, SLOT(slotSave()), Qt::UniqueConnection);
    connect(ui->actionAbout_Qt, SIGNAL(triggered()),
            qApp, SLOT(aboutQt()), Qt::UniqueConnection);
    connect(ui->actionAbout_program, SIGNAL(triggered()),
            this, SLOT(slotAboutProgram()), Qt::UniqueConnection);

    connect(ui->actionPr_eferences, SIGNAL(triggered()),
        mSettingsDialog, SLOT(show()), Qt::UniqueConnection);
    connect(mSettingsDialog, SIGNAL(accepted()),
        this, SLOT(slotPreferencesAccepted()), Qt::UniqueConnection);

    // Наприкінці, викликаємо слот для створення нового документу
    // Таким чином, користувач зможе одразу ж розпочати роботу
    slotNew();

    // Прочитати налаштування з файлу
    readSettings();

    // Завстосувати налаштування
    applySettings();
}

MainWindow::~MainWindow()
{
    // Видалити форму з пам'яті
    delete ui;
}

// Обробник події зміни стану віджета
// Подія зміни стану виникає у низці випадків: зміна батьківського віджета,
// зміна стану активності, зміна вигляду (шрифт, стилі) тощо
void MainWindow::changeEvent(QEvent *e)
{
    // Виклик обробника з бітьківського класу
    QMainWindow::changeEvent(e);

    switch (e->type()) {
    // Якщо відбулася зміна мови користувацького інтерфейсу...
    case QEvent::LanguageChange:
        ui->retranslateUi(this); // ... виконати переклад
        break;
    default:
        break;
    }
}

// Обробник події закривання вікна
void MainWindow::closeEvent(QCloseEvent *e)
{
    // Якщо вміст модифіковано...
    if (isWindowModified())
    {
        // ... запитати користувача про збереження документу
        if (!askForFileSaveAndClose())
        {
            // Якщо користувач натиснув "Відмінити"...
            // ... ігнорувати закриття - продовжувати роботу
            e->ignore();
            return;
        }
    }

    // Інакше прийняти подію - закрити вікно
    e->accept();
}

// Слот для створення нового документу
void MainWindow::slotNew()
{
    // Запитати користувача про збереження документу
    if (!askForFileSaveAndClose())
    {
        // Якщо користувач натиснув "Відмінити"...
        // ... ігнорувати виклик- продовжувати роботу
        return;
    }

    // Задати ім'я для нового файлу за замовчуванням
    mFileName = "UntitledFile";
    // Очистити текстове поле
    ui->plainTextEdit->clear();
    // Встановити - вміст не модифіковано
    setWindowModified(false);
    // Оновити заголовок вікна
    updateTitle();
}

// Слот для відкриття файлу у редакторі
void MainWindow::slotOpen()
{
    // Викликати системний діалог відкриття файлу
    // у домашній теці користувача
    QString lFileName = QFileDialog::getOpenFileName(this, tr("Open file..."), QDir::homePath(),
        tr("Text files (*.txt);;All files (*.*)")); // вказуємо фільтри для перегляду файлів

    // Якщо користувач не вибрав жодного файлу...
    if (lFileName.isEmpty())
    {
        // ... вийти з методу
        return;
    }

    // Запитати користувача про збереження документу
    if (!askForFileSaveAndClose())
    {
        // Якщо користувач натиснув "Відмінити"...
        // ... ігнорувати виклик- продовжувати роботу
        return;
    }

    // Встановлюємо ім'я відкритого файлу
    QFile lFile(lFileName);

    // Якщо текстовий файл відкрито для читання...
    if (lFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // задати ім'я для файлу
        mFileName = lFileName;
        // ... читаємо весь вміст та втановлюємо текст для редактора...
        ui->plainTextEdit->setPlainText(lFile.readAll());
        // ..закриваємо відкритий файл...
        lFile.close();
        // ...встановлюємо стан вікна - вміст не модифіковано...
        setWindowModified(false);
        // ... та оновлюємо заголовок вікна для зображення
        // назви поточоного відкритого файлу
        updateTitle();
    }
    else
    {
        // Якщо під час відкриття файлу виникла помилка...
        // виводимо діалогове вікно з повідомленням, куди підставляємо ім'я файлу,
        // вказуємо - вікно містить одну кнопку "Ок" та заголовок "Error"
        QMessageBox::warning(this, tr("Error"), QString(tr("Could not open file %1 for reading")).arg(lFile.fileName()), QMessageBox::Ok);
    }

}

// Слот для збереження змін у поточному файлі
void MainWindow::slotSave()
{
    // Якщо вміст не модифіковано...
    if (!isWindowModified())
    {
        // Вийти з методу - продовжити роботу
        return;
    }

    // Викликати системний діалог збереження файлу
    // у домашній теці користувача
    QString lFileName = QFileDialog::getSaveFileName(this, tr("Save file..."), QDir::homePath(),
        tr("Text files (*.txt);;All files (*.*)"));

    // Якщо користувач не вибрав
    // ім'я файлу для збереження...
    if (lFileName.isEmpty())
    {
        // ... вийти з методу
        return;
    }

    // Встановлюємо ім'я відкритого файлу
    QFile lFile(lFileName);

    // Якщо текстовий файл відкрито для запису...
    if (lFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        // Задати ім'я для файлу
        mFileName = lFileName;

        // Створюємо тимчасовий QByteArray для запису даних
        QByteArray lData;
        // Читаємо текст з редактора та додаємо QByteArray,
        // записуємо у файл та закриваємо файл після запису
        lData.append(ui->plainTextEdit->toPlainText());
        lFile.write(lData);
        lFile.close();

        // Встановлюємо стан вікна - вміст не модифіковано
        setWindowModified(false);
    }
    else
    {
        // Якщо під час відкриття файлу виникла помилка...
        // виводимо діалогове вікно з повідомленням, куди підставляємо ім'я файлу,
        // вказуємо - вікно містить одну кнопку "Ок" та заголовок "Error"
        QMessageBox::warning(this, tr("Error"), QString(tr("Could not open file %1 for writing")).arg(lFile.fileName()), QMessageBox::Ok);
    }
}

// Слот для відображення інформації про програму
void MainWindow::slotAboutProgram()
{
    // Виводимо діалогове інформаційне вікно з повідомленням, куди підставляємо
    // версію програми та назву програми які повертає QApplication.
    // Вказуємо - вікно містить заголовок "About"
    QMessageBox::about(this, tr("About"), QString("%1 v. %2")
        .arg(qApp->applicationName())
        .arg(qApp->applicationVersion()));

    //----Згадати про запуск qmake
}

void MainWindow::showPreferencesDialog()
{
    // Читаємо налаштування та встановлюємо
    // їх для діалога
    readSettings();

    // Показуємо діалог налаштувань
    mSettingsDialog->show();
}

void MainWindow::slotPreferencesAccepted()
{
    // Записати нові налаштування
    writeSettings();

    // Завстосувати налаштування
    applySettings();
}

// Метод оновлення заголовку вікна
void MainWindow::updateTitle()
{
    // Підставляємо у назву заголовку ім'я поточонго відкритого файлу
    // комбінацією символів "[*]" позначаємо місце, де буде виводитись
    // знак "*" у випадку, коли вміст вікна модифіковано
    QString lTitle = QString("TextEditor - %1[*]").arg(mFileName);

    // Встановлюємо заголовок вікна
    setWindowTitle(lTitle);
}

// Метод для перевірки поточного файла на зміни
// та виведення діалогу для користувача, з пропозицією
// зберегти зміни. Метод повертає логічне значення,
// яке містить false у випадку, коли коритувач натиснув
// у діалогу кнопку "Cancel"
bool MainWindow::askForFileSaveAndClose()
{
    // Якщо вміст вікна модифіковано...
    if (isWindowModified())
    {
        // викликаємо діалог з запитанням чи зберігати зміни:
        // підставляємо у текст діалогу назву поточного відкритого файлу,
        // задаємо кнопки: "Так", "Ні" та "Відмінити".
        // Результат роботи діалогу (натиснену кнопку) записуємо у змінну
        int lResult = QMessageBox::question(this, tr("Save changes"),
            QString(tr("File %1 is modified. Do you want to save your changes?")).arg(mFileName),
            QMessageBox::Yes, QMessageBox::No, QMessageBox::Cancel);

        // Якщо натиснено кнопку "Так"...
        if (QMessageBox::Yes == lResult)
        {
            slotSave(); // ... зберегти зміни
        }
        else
        {
            // Якщо натиснено кнопку "Відмінити"...
            if (QMessageBox::Cancel == lResult)
            {
                return false;
            }
        }
    }

    return true;
}

void MainWindow::readSettings()
{
    // Вказуємо де зберігалися налаштування
    // QSettings::NativeFormat - у форматі визначеному системою
    // QSettings::UserScope - налаштування для кожного користувача окремо
    // Твкож задаємо ім’я орагінзації та назву програми
    QSettings lSettings(QSettings::NativeFormat, QSettings::UserScope,
        "", qApp->applicationName());

    // Відкриваємо групу налаштувань
    lSettings.beginGroup(SETTINGS_GROUP_VIEW);

    // Читаємо налаштування
    bool lShowToolBar = lSettings.value(SETTING_SHOW_TOOLBAR, true).toBool();
    mSettingsDialog->setShowToolBar(lShowToolBar);

    bool lShowStatusBar = lSettings.value(SETTING_SHOW_STATUS_BAR, true).toBool();
    mSettingsDialog->setShowStatusBar(lShowStatusBar);
}

void MainWindow::writeSettings()
{
    // Вказуємо як зберігати налаштування
    // QSettings::NativeFormat - у форматі визначеному системою
    // QSettings::UserScope - берігати налаштування для кожного користувача окремо
    // Твкож задаємо ім’я орагінзації та назву програми
    QSettings lSettings(QSettings::NativeFormat, QSettings::UserScope,
        "", qApp->applicationName());

    // Відкриваємо групу налаштувань
    lSettings.beginGroup(SETTINGS_GROUP_VIEW);

    // Записуємо налаштування
    lSettings.setValue(SETTING_SHOW_TOOLBAR, mSettingsDialog->isShowToolBar());
    lSettings.setValue(SETTING_SHOW_STATUS_BAR, mSettingsDialog->isShowStatusBar());
}

void MainWindow::applySettings()
{
    // Читаємо налаштування встановлені
    // у діалозі та застосовуємо їх
    ui->mainToolBar->setVisible(mSettingsDialog->isShowToolBar());
    ui->statusBar->setVisible(mSettingsDialog->isShowStatusBar());
}
