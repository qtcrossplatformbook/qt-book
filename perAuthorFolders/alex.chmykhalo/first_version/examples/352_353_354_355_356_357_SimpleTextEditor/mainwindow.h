#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>

// Попреднє оголошення класу форми
// створеної з .ui-файлу
namespace Ui {
    class MainWindow;
}

class SettingsDialog;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *e);

private slots:
    void slotNew();
    void slotOpen();
    void slotSave();

    void slotAboutProgram();

    void showPreferencesDialog();
    void slotPreferencesAccepted();

private:
    void updateTitle();
    bool askForFileSaveAndClose();

    void readSettings();
    void writeSettings();
    void applySettings();

private:
    QString mFileName;

    // Вказівник на об'єкт форми
    // створеної з .ui-файлу
    Ui::MainWindow *ui;
    
    SettingsDialog *mSettingsDialog;
};

#endif // MAINWINDOW_H
