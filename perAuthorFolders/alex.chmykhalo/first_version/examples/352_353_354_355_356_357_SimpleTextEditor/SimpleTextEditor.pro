#-------------------------------------------------
#
# Project created by QtCreator 2013-10-29T04:20:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimpleTextEditor
TEMPLATE = app

MAJOR_VERSION = 1
MINOR_VERSION = 0

DEFINES += \
    MAJOR_VERSION=$$MAJOR_VERSION \
    MINOR_VERSION=$$MINOR_VERSION


SOURCES += main.cpp\
        mainwindow.cpp \
    settingsdialog.cpp

HEADERS  += mainwindow.h \
    settingsdialog.h

FORMS    += mainwindow.ui \
    settingsdialog.ui

RESOURCES += \
    resources/resources.qrc
