#include <QDebug>

// Власний тип даних - структура для комплексних чисел
struct complex
{
    double re;
    double im;
};

// Визначення потокового оператору для підтримки виводу типу
// complex з допомогою qDebug()
QDebug operator<<(QDebug dbg, const complex &c)
{
    dbg.nospace() << "(" << c.re << "+ i*" << c.im << ")";
    return dbg.space();
}

int main(int lArgc, char *lArgv[])
{
    // Вивід різноманітних типів даних
    qDebug() << "Hello, " << "this is debug output";
    qDebug() << "Integer values: " << 1 << 10 << 100;
    qDebug() << "Doubles and floats: " << .1 << .123 << 0.112345;
    qDebug() << "Characters: " << 'c' << '\t' << '$' << '\n' << "newline";
    qDebug() << "Booleans: " << true << false;
    qDebug() << "Pointers: " << lArgv;
    qDebug() << " and much more...";

    // Вивід власного типу даних
    complex c;
    c.re = 0.2;
    c.im = 1.5;
    qDebug() << "including custom types: " << c;

    return 0;
}
