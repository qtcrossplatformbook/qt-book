#include "ledindicator.h"

#include <QPainter>
#include <QPaintEvent>

// Радіус індикатора
const int cLedRadius = 7;

// Відступ між індикатором та надписом
const int cLedSpacing = 5;

LedIndicator::LedIndicator(QWidget *parent) :
    QWidget(parent),
    mIsTurnedOn(false) // Ініціалізуємо початковим значенням!
{
}

// Метод отримання значення - властивість text
QString LedIndicator::text() const
{
    return mText;
}

// Метод отримання значення - властивість turnedOn
bool LedIndicator::isTurnedOn() const
{
    return mIsTurnedOn;
}

// Метод встановлення значення - властивість text
void LedIndicator::setText(const QString &pText)
{
    mText = pText;
}

// Метод встановлення значення - властивість turnedOn
void LedIndicator::setTurnedOn(bool pIsTurnedOn)
{
    // Перевірка вже встановленого значення
    if (isTurnedOn() == pIsTurnedOn)
    {
        return;
    }

    mIsTurnedOn = pIsTurnedOn;

    // Емітуємо сигнал рпо зміну
    emit stateToggled(mIsTurnedOn);

    // Викликаємо метод QWidget::update()
    // який додає в чергу подій QPaintEvent
    // для того щоб перемалювати наш віджет
    // відповідно до встановленого isTurnedOn()
    update();
}

void LedIndicator::paintEvent(QPaintEvent *pEvent)
{
    // Створюємо об'єкт QPainter та вказуємо QPaintDevice  поточний віджет
    QPainter lPainter(this);

    // Використовуємо згладжування при малюванні
    // для кращого вигляду
    lPainter.setRenderHint(QPainter::Antialiasing);

    QPoint lLedCenter(cLedRadius + 1, height() / 2);

    QPainterPath lPath;
    lPath.addEllipse(lLedCenter, cLedRadius, cLedRadius);

    lPainter.save(); // Зберігаємо налаштування
                     // після усіх змін ми відновимо їх для
                     // малювання підпису

    // Створюємо радіальний (колами) градієнт
    // вказуємо центр для градієнта та радіус
    QRadialGradient lGradient(lLedCenter, cLedRadius);

    if (mIsTurnedOn) // Встановлюємо колір границі та градієнт
    {                // для ввімкненого та вимкненого станів

        // Задаємо об'єкт QPen - налаштування малювання контурів
        // Використовуємо констунту для задання кольору контура
        // в конструкторі QPen
        lPainter.setPen(QPen(Qt::darkGreen));

        // Задаємо колір в різних точках (0 - центр, 1 - край)
        // колір буде рівномірно змінюватись
        // Для задання кольору користумося текстовим шістнадцятковим
        // RGB позначенням - неявне перетворення в QColor
        lGradient.setColorAt(0.2, "#70FF70");
        lGradient.setColorAt(1, "#00CC00");
    }
    else
    {
        // Тут задаємо чорний колір
        // Конструктор QColor, значення червоної,
        // зеленої та синьої (0-255) компонент кольору
        lPainter.setPen(QPen(QColor(0, 0, 0)));

        lGradient.setColorAt(0.2, Qt::gray);
        lGradient.setColorAt(1, Qt::darkGray);
    }

    // Заповнюємо фігуру індикатора градієнтом
    lPainter.fillPath(lPath, QBrush(lGradient));

    // Малюємо границю індикатора
    lPainter.drawPath(lPath);

    // Відновлюємо налаштування перед останнім збереженням
    lPainter.restore();

    // Встановлюємо шрифт для малювання тексту
    // використовуємо QWidget::font(), щоб мати змогу
    // стилізувати надпис
    lPainter.setFont(font());

    // Квадрат у якому буде малюватися текст
    // QRect - клас для позначення прямокутника
    QRect lTextRect(cLedRadius * 2 + cLedSpacing, 0,
        width() - (cLedRadius * 2 + cLedSpacing), height());

    // Малюємо текст у заданому прямокутнику
    // вирівнювання по лівому краю та ветикально по центру
    lPainter.drawText(lTextRect,  Qt::AlignVCenter|Qt::AlignLeft, mText);
}

// Перевизначаємо віртуальний метод minimumSizeHint()
// для передачі корректних мінімальних розмірів
QSize LedIndicator::minimumSizeHint() const
{
    return QSize(cLedRadius * 2               // Діаметр індикатора
                 + fontMetrics().width(mText) // Ширина тексту mText
                 + cLedSpacing,               // Відступ

                 cLedRadius * 2);
}
