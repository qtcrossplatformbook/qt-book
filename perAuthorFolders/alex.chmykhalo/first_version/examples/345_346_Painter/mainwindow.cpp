#include "mainwindow.h"

#include <QHBoxLayout>
#include <QCheckBox>

#include "ledindicator.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    // Головне компонування
    QHBoxLayout *lLayout = new QHBoxLayout;
    setLayout(lLayout);

    // Створюємо наш індикатор та додаємо його до компонування
    LedIndicator *lLedIndicator = new LedIndicator;
    lLedIndicator->setText("LED Indicator");
    lLayout->addWidget(lLedIndicator);

    // Створюємо та додаємо прапорець
    QCheckBox *lCheckBox = new QCheckBox("Led ON");
    lLayout->addWidget(lCheckBox);

    // З'єднуємо прапорець та індикатор
    connect(lCheckBox, SIGNAL(toggled(bool)),
            lLedIndicator, SLOT(setTurnedOn(bool)), Qt::UniqueConnection);
}

MainWindow::~MainWindow()
{

}
