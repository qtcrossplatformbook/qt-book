#-------------------------------------------------
#
# Project created by QtCreator 2013-08-20T03:10:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Painter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ledindicator.cpp

HEADERS  += mainwindow.h \
    ledindicator.h
