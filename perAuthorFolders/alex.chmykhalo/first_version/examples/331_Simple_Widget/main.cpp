#include <QApplication>
#include <QLabel>

int main(int lArgc, char *lArgv[])
{
    // Створюємо об'єкт QApplication, який ініціалізує та налаштовує віконну
    // програму, керує її викоаннням з допомогою цикла обробки подій
    QApplication lApplication(lArgc, lArgv);

    QLabel lLabel;                          // Сворюємо віджет QLabel - мітку

    lLabel.setText("I am Widget!");         // Задаємо текст для мітки

    lLabel.setGeometry(200, 200, 300, 150); // Задаємо розміри - позицію (x, y)
                                            // ширину та висоту

    // Задаємо вирівнювання тексту
    lLabel.setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    // Клас QFont використовують для налаштування параметрів
    // шрифту. Вибираємо сімейсво шрифтів Arial Black та розмір 12.
    QFont lBlackFont("Courier New", 14);
    lBlackFont.setBold(true);

    lLabel.setFont(lBlackFont);             // Задаємо шрифт для мітки

    lLabel.show();                          // Викликаємо метод show(), щоб
                                            // показати мітку на екрані.

    lLabel.setWindowTitle("First GUI Project"); // Задаємо текст заголовку вікна

    return lApplication.exec(); // Запускаємо програму на виконання
                                // exec() виконує цикл обробки подій
                                // Програма очікує на дії користувача
                                // та виконує їх обробку.
}
