#include <QCoreApplication>

#include <QDebug>
#include <QFile>

int main(int argc, char *argv[])
{
    const QString lFileName("file.txt");

    // Перевяряємо чи файл існує
    if (!QFile::exists(lFileName))
    {
        qCritical("File %s does not exists.", qPrintable(lFileName));
        return 1;
    }

    QFile lFile;

    // Встановлюємо ім'я файлу
    lFile.setFileName(lFileName);

    // Відкриваємо файл - текстовий, тільки для читання
    if (!lFile.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        // Якщо файл не вдалося відкрити - виводимо повідомлення про помилку
        qCritical("Error %d : %s.", lFile.error(), qPrintable(lFile.errorString()));
        return 2;
    }

    // Допоки можна прочитати рядок
    while (lFile.canReadLine())
    {
        // ... виводити його у консоль
        qDebug() << lFile.readLine();
    }

    // Завершуємо роботу з файлом
    lFile.close();

    return 0;
}
