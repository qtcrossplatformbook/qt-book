/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   (c) Copyright Actum Solutions 1993..2010. All Rights Reserved.
 *
 *   File:          "qrclearablelineedit.h"
 *   Language:      C++, Qt
 *   Description:   Declaration of QRClearableLineEdit class, clearable line edit
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#ifndef QRCLEARABLELINEEDIT_H
#define QRCLEARABLELINEEDIT_H

#include <QLineEdit>

class QLabel;

class IconizedLineEdit : public QLineEdit
{
    Q_OBJECT
public:

    // Режими відображення піктограми,
    // які визначають її поведінку
    enum IconVisibilityMode {
        // Завжди відображати піктограму
        IconAlwaysVisible =0,
        // Відображати піктограму після наведення на поле вводу
        IconVisibleOnHover,
        // Відображати піктограму якщо текст присутній
        IconVisibleOnTextPresence,
        // Відображати піктограму якщо текст відсутній
        IconVisibleOnEmptyText,
        // Завжди ховати піктограму
        IconAlwaysHidden
    };

    explicit IconizedLineEdit(QWidget *parent = 0);

    bool isIconVisible() const;
    void setIconVisibility(IconVisibilityMode pIconVisibilityMode);

    void setIconPixmap(const QPixmap &pPixmap);
    void setIconTooltip(const QString &pToolTip);

    void setIconClickable(bool pIsIconClickable);

signals:
    void iconPressed();

protected:
    void resizeEvent(QResizeEvent *);
    bool eventFilter(QObject *pObject, QEvent *pEvent);

private slots:
    void slotTextChanged(const QString &pText);

private:
    void updateIconPositionAndSize();
    void updateTextMargins();

    void setIconVisible(bool pIsVisible);

private:
    QLabel *mIconLabel; // Вказівник на мітку, яка відображає піктограму
    IconVisibilityMode mIconVisibilityMode; // Режим відображення
    bool mIsIconCklickable;
};

#endif // QRCLEARABLELINEEDIT_H
