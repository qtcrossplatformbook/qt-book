#include "iconizedlineedit.h"

#include <QStyle>
#include <QLabel>
#include <QEvent>

// Конструктор класу
IconizedLineEdit::IconizedLineEdit(QWidget *parent) :
        QLineEdit(parent),
        mIsIconCklickable(false),
        mIconVisibilityMode(IconAlwaysVisible) // Ініціалізація
{
    mIconLabel = new QLabel(this); // Створюємо мітку для того,
                                   // щоб показати піктограму

    mIconLabel->installEventFilter(this); // Встановлюємо фільтр подій для піктограми
    installEventFilter(this);             // Встановлюємо фільтр подій для самого IconizedLineEdit

    // Обробка зміни тексту у полі
    connect(this, SIGNAL(textChanged(QString)),
        this, SLOT(slotTextChanged(QString)), Qt::UniqueConnection);
}

// Повертає true, якщо піктограма видима
bool IconizedLineEdit::isIconVisible() const
{
    return mIconLabel->isVisible();
}

// Встановлює режим відображення
// для піктограми
void IconizedLineEdit::setIconVisibility(IconVisibilityMode pIconVisibilityMode)
{
    // Збереженя режиму
    mIconVisibilityMode = pIconVisibilityMode;

    // Робимо зміни відповідно
    // до встановленого значення
    switch (pIconVisibilityMode)
    {
    case IconAlwaysVisible:
        setIconVisible(true);
        break;
    case IconVisibleOnEmptyText:
    case IconVisibleOnTextPresence:
        slotTextChanged(text());
        break;
    default:
        setIconVisible(false);
        break;
    }
}

// Встановлює піктограму
void IconizedLineEdit::setIconPixmap(const QPixmap &pPixmap)
{
    // Встановлюємо піктограму для мітки
    mIconLabel->setPixmap(pPixmap);

    // Оновлюємо позицію та розміри
    updateIconPositionAndSize();
}

// Встановлюємо підказку для піктограми
void IconizedLineEdit::setIconTooltip(const QString &pToolTip)
{
    // Підказку буде видно після наведення курсору
    // на мітку з піктограмою
    mIconLabel->setToolTip(pToolTip);
}

// Встановити режим реакції на натискання
// мишкою на піктограму
void IconizedLineEdit::setIconClickable(bool pIsIconClickable)
{
    mIsIconCklickable = pIsIconClickable;

    // Встановлюємо вигляд курсору
    // при наведенні на мітку з піктограмою
    if (mIsIconCklickable)
    {
        mIconLabel->setCursor(Qt::PointingHandCursor);
    }
    else
    {
        mIconLabel->setCursor(Qt::ArrowCursor);
    }
}

// Подія зміни розміру віджета
void IconizedLineEdit::resizeEvent(QResizeEvent *pEvent)
{
    // Якщо відбулася зміна розміру,
    // оновити позицію та розмір піктограми
    updateIconPositionAndSize();

    QWidget::resizeEvent(pEvent);
}

// Фільтр подій
bool IconizedLineEdit::eventFilter(QObject *pObject, QEvent *pEvent)
{
    // Якщо подія адресована мітці  піктограмою
    // та тип події QEvent::MouseButtonPress (клавішу мишки натиснули)
    if ((pObject == mIconLabel) && (pEvent->type() == QEvent::MouseButtonPress))
    {
        // ... та піктограма має реагувати на натиснення
        if (mIsIconCklickable)
        {
            emit iconPressed();
        }
    }

    // Не відфільтровуємо
    // жодну з подій
    return false;
}

// Реалізує реакцію на зміну тексту у полі
// для режимів IconVisibleOnEmptyText та IconVisibleOnNotEmptyText
void IconizedLineEdit::slotTextChanged(const QString &pText)
{
    if (IconVisibleOnEmptyText == mIconVisibilityMode)
    {
        setIconVisible(pText.isEmpty());
    }
    else if (IconVisibleOnTextPresence == mIconVisibilityMode)
    {
        setIconVisible(!pText.isEmpty());
    }
}

void IconizedLineEdit::updateIconPositionAndSize()
{
    // Оновити розмір піктограми
    mIconLabel->setScaledContents(true);
    mIconLabel->setFixedSize(height(), height());

    // Оновити розміщення піктограми
    QSize lSize = mIconLabel->size();
    mIconLabel->move(rect().right() - lSize.width(),(rect().bottom() + 1 - lSize.height())/2);

    // Оновити відступи
    updateTextMargins();
}

// Змінити відступи тексту всередині поля вооду
// в залежності від видимості мітки та її розміру
void IconizedLineEdit::updateTextMargins()
{
    if (mIconLabel->isVisible())
    {
        // Додати відступ справа
        // щоб текст не накладався на піктограму
        QMargins lMargins = textMargins();
        lMargins.setRight(mIconLabel->size().width() + 1);
        setTextMargins(lMargins);
    }
    else
    {
        // Забрати відступи
        setTextMargins(QMargins(0,0,0,0));
    }
}

// Зробити піктограму видимою чи приховати
void IconizedLineEdit::setIconVisible(bool pIsVisible)
{
    // Показати/сховати мітку з піктограмою
    mIconLabel->setVisible(pIsVisible);

    // Оновити відступи
    updateTextMargins();
}
