QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = IconizedLineEditExample
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    iconizedlineedit.cpp

HEADERS  += mainwindow.h \
    iconizedlineedit.h

FORMS    +=

