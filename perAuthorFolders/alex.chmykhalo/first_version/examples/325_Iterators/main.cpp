#include <QCoreApplication>

#include <QMap>
#include <QHash>

#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QList<QString> lStringList;
    lStringList << "aa" << "bb" << "cc";

    // Для кожного рядка lString у списку lStringList
    foreach (const QString &lString, lStringList)
    {
        qDebug() << lString; // ...виводити рядок у консоль
    }

    QList<int> lList; // Створюємо список цілих чисел
    lList.append(3); // Додаємо елементи
    lList.append(6);
    lList.append(9);

    QListIterator<int> lIt(lList); // Створюємо ітератор для списку
    while (lIt.hasNext()) // Поки наступний елемент існує
    {
        qDebug() << lIt.next(); // ...виводити наступний елемент
    }

    // -=-=-=-=-=-=

    QHash<QString, int> lNumberByName;
    lNumberByName.insert("twelve", 12);
    lNumberByName.insert("thirty three", 33);
    lNumberByName.insert("one hundred an twenty five", 125);

    QHashIterator<QString, int> lHashIterator(lNumberByName);
    lHashIterator.toBack(); // Перейти до кінця контейнеру -
                            // ітератор вказує після останнього елемента
    while (lHashIterator.hasPrevious())
    {
        lHashIterator.previous(); // Переходимо до попереднього елементу

        // Виводимо ключ та значення
        qDebug() << lHashIterator.key()
                 << " - "
                 << lHashIterator.value();
    }

    QHash<QString, int>::const_iterator lStlLikeIterator;
    for (lStlLikeIterator = lNumberByName.begin();
         lStlLikeIterator != lNumberByName.end();
         lStlLikeIterator++)
    {
        qDebug() << lStlLikeIterator.key()
                 << " - "
                 // Теж саме що і (*lStlLikeIterator)
                 << lStlLikeIterator.value();
    }

    // -=-=-=-=-=-=-=-=-=-=


    QMap<QString, QString> lSurnameByName;
    lSurnameByName.insert("Bill", "Hunter");
    lSurnameByName.insert("Marry", "Lee");

    // Пошук значення за ключем
    qDebug() << "Bill" << lSurnameByName.value("Bill");
    qDebug() << "Marry" << lSurnameByName.value("Marry", "Doe");

    // Додаємо інше значення з уже існуючим ключем
    lSurnameByName.insert("Marry", "Hunter");
    qDebug() << "Marry" << lSurnameByName.value("Marry");

    // Ключі не існують - вивід значень за замовчуванням
    qDebug() << "James" << lSurnameByName.value("James");
    qDebug() << "John" << lSurnameByName.value("John", "Doe");

    QHash<QString, QString> lClassificationHash;

    // Додаємо кілька значень з однаковими ключами
    lClassificationHash.insertMulti("fruits", "apple");
    lClassificationHash.insertMulti("fruits", "orange");
    lClassificationHash.insertMulti("vegetables", "potato");
    lClassificationHash.insertMulti("vegetables", "cabbage");
    lClassificationHash.insertMulti("vegetables", "tomato");

    // Вивід одного значення за ключем
    qDebug() << lClassificationHash.value("fruits");

    // Вивід значеннь з ключем
    qDebug() << lClassificationHash.values("fruits");
    qDebug() << lClassificationHash.values("vegetables");

    return a.exec();
}
