#-------------------------------------------------
#
# Project created by QtCreator 2013-07-04T10:34:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Calc
TEMPLATE = app


SOURCES += main.cpp\
        calculatormainwindow.cpp

HEADERS  += calculatormainwindow.h

FORMS    += calculatormainwindow.ui

RESOURCES += \
    res.qrc
