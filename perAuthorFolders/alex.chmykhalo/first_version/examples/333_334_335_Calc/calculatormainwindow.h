#ifndef CALCULATORMAINWINDOW_H
#define CALCULATORMAINWINDOW_H

#include <QWidget>

// Попереднє оголошення класів
class QSignalMapper;
class QPushButton;
class QLCDNumber;

class CalculatorMainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CalculatorMainWindow(QWidget *parent = 0);
    ~CalculatorMainWindow();

private:

private slots:
    void slotClear(); // Обробка натискання кнопки скидання
    void slotButtonPressed(int pNum); // Обробка цифрових кнопок
    void slotPlusEqual(); // Обробка кнопки сумування/виведення результату

private:
    // Окремий метод для створення інтерфейсу програми
    void createWidgets();

private:
    int mSum; // Результат
    int mNextNumber; //Наступний доданок

    QSignalMapper *mMapper;

    // Цифрові кнопки
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;

    // Кнопка виводу результату та сумування
    QPushButton *pushButtonPlus;

    // Кнопка скидання результату
    QPushButton *pushButtonC;

    // Віджет - цифровий дисплей
    QLCDNumber *lcdNumber;

};

#endif // CALCULATORMAINWINDOW_H
