#include <QDebug>

#include <QString>
#include <QStringList>

#include <QStringBuilder>//можна використовувати для ефективнішої конкатенації стрічок

int main(int lArgc, char *lArgv[])
{
    QString lMainStr = "string"; //lMainStr == "string"

    lMainStr += ' '; //lMainStr == "string "

    (lMainStr += "is") += ' '; //lMainStr == "string is"

    QString lHelperStr1("composed");
    lMainStr += lHelperStr1; // lMainStr == "string is composed"

    QString lHelperStr2 = + ' ' +QString("from") + ' ';
    lMainStr.append(lHelperStr2);
    // lMainStr == "string is composited from "

    lMainStr.push_back("fragments");
    // lMainStr == "string is composited from fragments"

    lMainStr.prepend("This ");
    //lMainStr == "This string is composited from fragments"

    lMainStr.insert(lMainStr.length(), ".");
    //lMainStr == "This string is composited from fragments."

    lMainStr += QString(2, '.');
    //lMainStr == "This string is composited from fragments..."

    lMainStr = lMainStr.rightJustified(lMainStr.length() + 8, ' ');
    //lMainStr == "        This string is composited from fragments..."

    qDebug() << lMainStr;

    QString hello("Hello");
    QStringRef el(&hello, 2, 3);
    QLatin1String world("world");
    QString message =  hello % el % world % QChar('!');
    qDebug() << message;
    //message = "Hellolloworld!"

    //Розбиття рядка на фрагменти (приклад)

    QString lQuote = "This is sentence one. This is sentence two.";

    //нова стрічка з п’яти перших символів
    QString lFragment1 = lQuote.left(5); // lFragment1 == "This "
    qDebug() << "lFragment1 is: " << lFragment1;

    // Перше речення: Всі символи до (першої) крапки
    QString lSentence = lQuote.section('.', 0, 0);
    qDebug() << "lSentence is: " << lSentence;
    // lSentence == "This is sentence one"

    // Cписок слів у рядку
    QStringList lWordsList = lSentence.split(' ', QString::SkipEmptyParts);
    qDebug() << lWordsList;
    // lWordsList == ("This", "is", "sentence", "one", "This", "is", "sentence", "two")

    // Перетворення чисел в рядки та навпаки (приклад)
    // перетворення цілого числа в рядок
    int x = 16;
    QString lXStr = QString::number(x);
    // x = 7; lXStr  = "7"
    qDebug() << x << "==" << lXStr;

    //перетворення рядка в ціле число
    int y = lXStr.toInt();
    qDebug() << x << "==" << y;

    //перетворення дробового числа в рядок
    double teta = 12099.10012021210102109991;

    // неформатований вивід
    QString lTetaStr = QString::number(teta);
    // lTetaStr == "12099.1"

    qDebug() << lTetaStr;

    // або так
    lTetaStr.setNum(teta);
    // lTetaStr == "12099.1"

    qDebug() << lTetaStr;

    //вивід із 4-ма знаками після коми
    lTetaStr = QString::number(teta,'f',4);
    // lTetaStr == "12099.1001"

    qDebug() << lTetaStr;

    //форматування з використанням символа 'e'
    lTetaStr = QString::number(teta,'e');
    // lTetaStr == "1.209910e+04"

    qDebug() << lTetaStr;

    // Запис числа у рядок в різних системах числення
    lXStr = QString("int %1 is %L2 in desimal system, %L3 in binary system, and %L4 in hexadecimal")
            .arg(x)
            .arg(x, 0, 10)
            .arg(x, 0, 2)
            .arg(x, 0, 16);

    //при перетворенні нечислової стрічки в число відповідна функція поверне 0
    QString lText = "lalala";
    int z = lText.toInt();
    qDebug() << lText << "!=" << z;



    qDebug() << lXStr;


    // Перетворення в рядки STL та назад (приклад)
    QString lQtStringInitial = "I am a standard STL string.";
    std::string lStdString = lQtStringInitial.toStdString();
    QString lQtString = QString::fromStdString(lStdString);
    qDebug() << lQtString;

    // Перевірка на пустий рядок. Пусті та нульові (null) рядки.
    QString().isNull();           // true (нульовий рядок)
    QString().isEmpty();          // true (нульовий рядок є також пустим)

    QString("").isNull();         // false (пустий рядок не є нульовим)
    QString("").isEmpty();        // true

    QString("abc").isNull();      // false
    QString("abc").isEmpty();     // false

    // Приклад  використанння макросу qPrintable
    qWarning("%s: %s", qPrintable("key"), qPrintable("value"));
}
