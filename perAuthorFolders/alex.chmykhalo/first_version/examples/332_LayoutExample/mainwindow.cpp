#include "mainwindow.h"

#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    // Перший горизонтальний рядок

    // Початковий текст у полі вводу
    QLineEdit *lLineEdit = new QLineEdit("Text 1");

    // Задаємо текст
    // & - означає комбінацію клавіш
    // для активації віджета
    QLabel *lLabel = new QLabel("Line Edit &1");


    // Задаємо віджет до якого буде пермикатися
    // фокус вводу при натисканні Alt+1
    lLabel->setBuddy(lLineEdit);

    // Розташовуємо поле вводу та
    // мітку у одному рядку.
    QHBoxLayout *lHBoxLayout = new QHBoxLayout;
    lHBoxLayout->addWidget(lLabel);
    lHBoxLayout->addWidget(lLineEdit);

    // Другий горизонтальний рядок
    QLineEdit *lLineEdit2 = new QLineEdit("Text 2");
    QLabel *lLabel2 = new QLabel("Line Edit &2");
    lLabel2->setBuddy(lLineEdit2);
    QHBoxLayout *lHBoxLayout2 = new QHBoxLayout;
    lHBoxLayout2->addWidget(lLabel2);
    lHBoxLayout2->addWidget(lLineEdit2);

    // Третій ряд віджетів з кнопками
    QPushButton *lPushButtonOk = new QPushButton("&Ok");
    QPushButton *lPushButtonCancel = new QPushButton("&Cancel");
    QHBoxLayout *lHBoxLayout3 = new QHBoxLayout;
    // Додаємо елемент-розтягнення
    // він займе весь можливий вільний простір
    // та "притисне" кнопки до краю
    lHBoxLayout3->addStretch();
    lHBoxLayout3->addWidget(lPushButtonOk);
    lHBoxLayout3->addWidget(lPushButtonCancel);

    // Додаємо компонування вертикально у колонку
    QVBoxLayout *lVBoxLayout = new QVBoxLayout;
    lVBoxLayout->addLayout(lHBoxLayout);
    lVBoxLayout->addLayout(lHBoxLayout2);
    lVBoxLayout->addLayout(lHBoxLayout3);

    // Задаємо компонування для вікна
    setLayout(lVBoxLayout);
}

MainWindow::~MainWindow()
{

}
