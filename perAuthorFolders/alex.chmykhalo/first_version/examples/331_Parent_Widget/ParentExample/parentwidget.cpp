#include "parentwidget.h"

#include <QLabel>
#include <QPushButton>
#include <QLineEdit>

ParentWidget::ParentWidget(QWidget *parent) :
    QWidget(parent)
{
    // Створюємо мітку
    QLabel *lLabel = new QLabel(this); // Задаємо батьківський віджет - this
                                       // тобто екземпляр класу ParentWidget
    lLabel->setGeometry(50, 0, 200, 30); // Позиція відносно лівого верхнього
                                         // кута батьківського віджета
    lLabel->setText("Text Label");     // Текст на мітці

    // Створюємо кнопку, задаємо "батька", геометрію та текст
    QPushButton *lPushButton = new QPushButton(this);
    lPushButton->setGeometry(50, 50, 200, 30);
    lPushButton->setText("PushButton");

    // Створюємо поле вводу, задаємо "батька", геометрію та текст
    QLineEdit *lLineEdit = new QLineEdit(this);
    lLineEdit->setGeometry(50, 100, 200, 30);
    lLineEdit->setText("LineEdit");
    lLineEdit->selectAll(); // Виділяємо текст у полі вводу(просто для прикладу)

    // Врешті решт змінюємо розміри батьківського віджета
    setGeometry(200, 200, 300, 150);
    // та встановлюємо текст заголовку вікна
    setWindowTitle("Parent Widget Example");
}
