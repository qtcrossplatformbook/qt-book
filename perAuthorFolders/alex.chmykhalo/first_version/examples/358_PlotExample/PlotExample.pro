#-------------------------------------------------
#
# Project created by QtCreator 2013-08-09T07:41:49
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(3rdparty/qwt/qwt.pri)

TARGET = PlotExample
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
