INCLUDEPATH += $$PWD/include

win32 {
    LIBS += -L$$PWD/lib/win32
}

unix {
    LIBS += -L$$PWD/lib/unix
}

LIBS += -lqwt
