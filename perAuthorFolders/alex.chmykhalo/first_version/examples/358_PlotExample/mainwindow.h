#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
//    class MainWindow;
}

class QwtPlot;
class QSignalMapper;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private slots:
    void slotAddPoint();

private:
    void createMainMenu();

    void slotSwitchLanguage(const QString &pLanguage);

private:
    QwtPlot *lPlot;
    QSignalMapper *mLanguageSignalMapper;
//    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
