#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "qwt_plot.h"
#include "qwt_plot_panner.h"
#include "qwt_plot_magnifier.h"
#include "qwt_plot_canvas.h"
#include <QVBoxLayout>

#include <QAction>
#include <QMenuBar>
#include <QSignalMapper>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)//,
//    ui(new Ui::MainWindow)
{
//    ui->setupUi(this);

    setCentralWidget(new QWidget);

    QLayout *lLayout = new QVBoxLayout;
    centralWidget()->setLayout(lLayout);

    QwtPlot *lPlot = new QwtPlot;
    lLayout->addWidget(lPlot);

    QwtPlotCanvas *canvas = new QwtPlotCanvas();
    canvas->setFrameStyle(QFrame::Box|QFrame::Plain);
    lPlot->setCanvas(canvas);

    lPlot->setAxisTitle(QwtPlot::xBottom, "x");
    lPlot->setAxisScale(QwtPlot::xBottom, -10.0, 10.0);

    lPlot->setAxisTitle(QwtPlot::yLeft, "y" );
    lPlot->setAxisScale(QwtPlot::yLeft, -10.0, 10.0);

    new QwtPlotPanner( canvas );
    new QwtPlotMagnifier( canvas );

    createMainMenu();

}

MainWindow::~MainWindow()
{
//    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
//        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::slotAddPoint()
{
    qDebug("Add point..");
}

void MainWindow::createMainMenu()
{
    QMenuBar *lMenuBar = new QMenuBar;
    setMenuBar(lMenuBar);

    QMenu *lFileMenu = lMenuBar->addMenu(tr("&File"));
    lFileMenu->addAction(tr("&Exit"), this, SLOT(close()));

    QAction *lActionAddPoint = lFileMenu->addAction(tr("&Add..."));
    connect(lActionAddPoint, SIGNAL(triggered()),
            this, SLOT(slotAddPoint()), Qt::UniqueConnection);

//    QMenu *lLanguageMenu = lMenuBar->addMenu(tr("&Language"));


//    QAction *lActionLangualeEn = lFileMenu->addAction(tr("En"));
//    QAction *lActionLanguageUa = lFileMenu->addAction(tr("Ua"));

//    mLanguageSignalMapper = new QSignalMapper(this);
//    mLanguageSignalMapper->setMapping(lActionLangualeEn, "en");
//    mLanguageSignalMapper->setMapping(lActionLanguageUa, "ua");
}

void MainWindow::slotSwitchLanguage(const QString &pLanguage)
{
//    QApplication *lApplication =
}
