#include <QCoreApplication>

#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    int error_num = 59;
    std::string error_string("uknown error");

    qDebug("result: %d, description: %s", error_num, error_string.c_str());

    qDebug() << "result: "
             << error_num
             << ", description:"
             << error_string.c_str();

    qWarning("warning: %d, description: %s",
             error_num, error_string.c_str());

    qWarning() << "warning: "
             << error_num
             << ", description:"
             << error_string.c_str();

    qCritical("critical error: %d, description: %s",
              error_num, error_string.c_str());

    qCritical() << "critical error: "
             << error_num
             << ", description:"
             << error_string.c_str();

    qFatal("fatal error: %d, description: %s",
           error_num, error_string.c_str());

    return a.exec();
}
