import QtQuick 2.3

Rectangle {
    id: timerUi
    width: 360
    height: 360
    color: 'black'

    property int seconds: 0
//    onSecondsChanged: {
//        console.log(seconds)
//    }

    Column {
        anchors.centerIn: parent
        spacing: 20

        Text {
            id: timeDisplay
            anchors.horizontalCenter: parent.horizontalCenter
            color: "#58b965"
            text: parent.lalala(parent.height)
//            text: "0:00:00"
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            style: Text.Raised
            font.pointSize: 40
        }
        function lalala(x) {
            return x
        }

        Timer {
            id: timer
            interval: 1000
            running: false
            repeat: true
            onTriggered: {
                seconds ++

                var sec = seconds % 60
                if (sec < 10) {sec = "0" + sec;}

                var min = Math.floor(seconds / 60)
                if (min < 10) {min = "0"+min;}

                var hour = Math.floor(seconds / 3600)
                timeDisplay.text = hour + ':' + min + ':' + sec
            }
        }

        Row {
            spacing: 20

            Button {
                id: startAndPauseButton
                text: 'Start'
                onClicked: {
                    if (timer.running) {
                        timerUi.state = "paused";
                    }
                    else{
                        timerUi.state = "running";
                    }
                }
            }

            Button {
                id: resetButton
                text: 'Reset'
                onClicked: {
                    timerUi.state = "ready"
                    timerUi.seconds = 0
                    timeDisplay.text = "0:00:00"
                }
            }            
        }   
    }

    states: [
        State {
            name: "ready"

            PropertyChanges { target: timer; running: false }
            PropertyChanges { target: startAndPauseButton; text:"Start" }
        },
        State {
            name: "running"

            PropertyChanges { target: startAndPauseButton; text:"Pause" }
            PropertyChanges { target: timer; running: true }
        },
        State {
            name: "paused"

            PropertyChanges { target: startAndPauseButton; text:"Resume" }
            PropertyChanges { target: timer; running: false }
        }
    ]

}

