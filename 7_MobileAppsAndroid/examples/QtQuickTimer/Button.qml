import QtQuick 2.3

Rectangle {
    readonly property color startColor: "#7cc052"
    readonly property color endColor: "#5d9125"

    property alias buttonPressed: buttonMouseArea.pressed
    property alias text: buttonText.text

    signal clicked

    width: 100
    height: 30
    color: "red"
    border {
        color: buttonPressed ? Qt.darker(endColor) : Qt.lighter(startColor)
        width: 2
    }
    radius: 10

    gradient: Gradient {
        GradientStop {
            position: 0.00;
            color: buttonPressed ? endColor : startColor;
        }
        GradientStop {
            position: 1.00;
            color: buttonPressed ? startColor : endColor;
        }
    }

    Text {
        id: buttonText
        anchors.centerIn: parent
        font {
            bold: true
            pointSize: 10
        }
        color: 'white'
    }

    MouseArea {
        id: buttonMouseArea
        anchors.fill: parent

        // hovered property

        onClicked: {
            parent.clicked()
        }
    }
}
