import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtSensors 5.3

ApplicationWindow {
    title: qsTr("Compass Example")
    width: 640
    height: 480
    visible: true

    FontLoader {
        id: monotype
        source: "qrc:/fonts/Monotype-Corsiva-Regular.ttf"
        name: "Monotype Corsiva Regular"
    }

    Compass {
        id: compass

        property int azimuth: 0

        active: true

        axesOrientationMode: Compass.UserOrientation

        onReadingChanged: {
            var v = Math.round(reading.azimuth)
            if (v < 0)
            {
                v = 360 + v
            }
            if (v !== compass.azimuth)
            {
                compass.azimuth = v
            }
        }
    }

    Rectangle {
        id: content
        anchors.fill: parent
        border.color: "transparent"
        border.width: 10
        anchors.bottomMargin: 30
        color: "transparent"
        Rectangle {
            id: background
            anchors.centerIn: parent
            height: parent.width < parent.height ? parent.width - parent.border.width
                                                : parent.height - parent.border.width
            width: height
            color: "transparent"
            Image {
                id: backgroundImage
                anchors.fill: parent
                source: "qrc:/images/digital-compass_background.svg"
            }

            Rectangle {
                id: grad
                anchors.centerIn: parent
                height: parent.height - parent.height / 2.45
                width: height
                color: "transparent"
                Image {
                    id: gradImage
                    anchors.fill: parent
                    source: "qrc:/images/digital-compass_grad.svg"
                }
                rotation: -compass.azimuth
            }

            Rectangle {
                id: arrow
                anchors.centerIn: parent
                height: parent.height - parent.height / 1.95
                width: height / 7
                color: "transparent"
                Image {
                    id: arrowImage
                    anchors.fill: parent
                    source: "qrc:/images/compass_arrow.svg"
                }
                rotation: -compass.azimuth
            }
        }

        Text {
            id: azimuthText
            anchors.top: background.bottom
            anchors.horizontalCenter: background.horizontalCenter
            anchors.topMargin: 10
            font.pixelSize: 20
            font.family: monotype.name
            text: compass.azimuth
        }
    }
}
